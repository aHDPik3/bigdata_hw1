package hadoop_wordcount;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * Class for testing wether string consists only of ASCII characters
 */
public class StringUtils {
    /**
     * ASCII encoder (static member for String testing)
     */
    static CharsetEncoder asciiEncoder =
            Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

    /**
     * Method checks whether string consists only of ASCII symbols
     * @param v String tp check
     * @return true if consists only of ASCII symbols, false otherwise
     */
    public static boolean isPureAscii(String v) {
        return asciiEncoder.canEncode(v);
    }
}
