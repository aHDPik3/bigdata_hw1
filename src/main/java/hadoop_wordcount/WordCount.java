package hadoop_wordcount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Main class
 */
public class WordCount {

    /**
     * Mapper Class
     */
    public static class TokenizerMapper
            extends Mapper<LongWritable, Text, IntWritable, Text> {
        /**
         * private field for storing current word
         */
        private Text word = new Text();

        /**
         * Map function
         * @param key Is not used
         * @param value Text line to parse
         * @param context Data context
         * @throws IOException
         * @throws InterruptedException
         */
        public void map(LongWritable key, Text value, Context context
        ) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            while (itr.hasMoreTokens()) {
                word.set(itr.nextToken());
                if (StringUtils.isPureAscii(word.toString()))
                    context.write(new IntWritable(word.getLength()), word);
            }
        }

    }

    /**
     * Reducer class
     */
    public static class IntSumReducer
            extends Reducer<IntWritable, Text, IntWritable, Text> {

        /**
         * Maximum word length found
         */
        private int maxLength;
        /**
         * Longest word found
         */
        private Text maxText;

        /**
         * Initialization
         * @param context Data context
         * @throws java.io.IOException
         * @throws InterruptedException
         */
        protected void setup(Context context) throws java.io.IOException, InterruptedException {

            maxText = new Text();
            maxLength = 0;
        }

        /**
         * Reduce method
         * @param key word length
         * @param values all words with corresponding length
         * @param context Data context
         * @throws IOException
         * @throws InterruptedException
         */
        public void reduce(IntWritable key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            if (key.get() > maxLength) {
                maxLength = key.get();
                maxText = values.iterator().next();
            }
        }

        /**
         * Method to call upon finish (writes the longest word)
         * @param context Data context
         * @throws IOException
         * @throws InterruptedException
         */
        protected void cleanup(Context context) throws IOException, InterruptedException {

            context.write(new IntWritable(maxLength), maxText);

        }

    }

    /**
     * Main method (entry)
     * @param args first is input file, second is output file
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapred.textoutputformat.separator", ",");
        Job job = Job.getInstance(conf, "word count");
        job.setJarByClass(WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }
}