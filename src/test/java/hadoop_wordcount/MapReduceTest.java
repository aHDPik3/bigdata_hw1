package hadoop_wordcount;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit Test Class
 */
public class MapReduceTest {
    /**
     * Class For Mapper testing
     */
    MapDriver<LongWritable, Text, IntWritable, Text> mapDriver;
    /**
     * Class for Reducer testing
     */
    ReduceDriver<IntWritable,Text,IntWritable,Text> reduceDriver;
    /**
     * Class for Mapper Reducer pipeline testing
     */
    MapReduceDriver<LongWritable, Text, IntWritable,Text,IntWritable,Text> mapReduceDriver;

    /**
     * Initialization
     */
    @Before
    public void setUp() {
        WordCount.TokenizerMapper mapper = new WordCount.TokenizerMapper();
        WordCount.IntSumReducer reducer = new WordCount.IntSumReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver( reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    /**
     * Mapper Test Function (standard input)
     * @throws IOException
     */
    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(), new Text(
                "lol loool"));
        mapDriver.withOutput(new IntWritable(3),new Text("lol")).withOutput(new IntWritable(5),new Text("loool"));
        mapDriver.runTest();
    }

    /**
     * Reducer Test function (standard input)
     * @throws IOException
     */
    @Test
    public void testReducer() throws IOException {

        List<Text> values = new ArrayList<Text>();
        values.add(new Text("papa"));
        values.add(new Text("mama"));
        reduceDriver.withInput(new IntWritable(4), values);
        reduceDriver.withOutput(new IntWritable(4), new Text("papa"));
        reduceDriver.runTest();
    }

    /**
     * Mapper Reducer pipeline test (ASCII and non-ASCII words)
     * @throws IOException
     */
    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "poodle vodka чебурашка"));
        mapReduceDriver.withOutput(new IntWritable(6), new Text("poodle"));
        mapReduceDriver.runTest();
    }

    /**
     * Mapper Reducer pipeline test (non-ASCII words)
     * @throws IOException
     */
    @Test
    public void testEmptyMapReduce() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "чебурашка"));
        mapReduceDriver.runTest();
    }
}
